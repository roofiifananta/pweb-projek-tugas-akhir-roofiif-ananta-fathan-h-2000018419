<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=
    Poppins:wght@600&display=swap" rel="stylesheet"> 
    <link rel="stylesheet" href="tamu.css">
    <script src="https://kit.fontawesome.com/be118125cf.js" crossorigin="anonymous"></script>
    <title>Input Data Tamu</title>
</head>
<body>
<div class="login-container">
    <form action="input-data.php" name="tamu" method="post"> 
    <h2>Buku Tamu</h2>
    <div class="input-div">
        <div class="i">
            <i class="fas fa-key"></i>
        </div>
        <div>
            <h5>ID</h5>
            <input class="input" name="id" type="text">
        </div>
    </div>
    <div class="input-div">
        <div class="i">
            <i class="fas fa-user"></i>
        </div>
        <div>
            <h5>Nama</h5>
            <input class="input" name="nama" type="text">
        </div>
    </div>
    <div class="input-div">
        <div class="i">
            <i class="fas fa-envelope"></i>
        </div>
        <div>
            <h5>Email</h5>
            <input class="input" name="email" type="text">
        </div>
    </div>
    <div class="input-div">
        <div class="i">
            <i class="fas fa-address-book"></i>
        </div>
        <div>
            <h5>Alamat</h5>
            <input class="input" name="alamat" type="text">
        </div>
    </div>
    <div class="input-div">
        <div class="i">
            <i class="fas fa-phone-square-alt"></i>
        </div>
        <div>
            <h5>Telepon</h5>
            <input class="input" name="telp" type="text">
        </div>
    </div>
    <input class="btn" type="submit" name="submit" value="Sumbit">
    <input class="btnn" type="reset" name="batal" value="Batal">
    </div>
    </div>
    </form>
    <footer>
        <p class="copyright">Copyright by Roofiif Ananta</p>
    </footer>
</div>
</body>
</html>